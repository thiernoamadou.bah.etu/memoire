## From monolithic to micro-services architectures : challenges and perspectives

  1)  Introduction 		 
  2)  Architecture monolithique
      2.1) Définition
      2.2) Avantages
      2.3) Inconvénients
  3) Architecture microservices
      3.1) Définition
      3.2) Avantages
      3.3) Inconvénients
      
### Contexte :  

   La plupart des entreprises en pleine évolution, transitent de l'architecture monolithique à l'architecture des microservices. Vue qu'une architecture de microservices est composé en des petits services autonomes, où chaque service peut être développé et testé indépendamment des autres, le déploiement du code est plus facile par rapport à l'architecture monolithique où un petit changement apporté au code nécessite le redéploiement de l'ensemble de l'application. 
   
   L'objectif de ce travail, est de déterminer les raisons qui poussent les entreprises de migrer de l'architecture monolithique à l'architecture des microservices, et de déterminer  les défis auxquels les entreprises peuvent être confrontées lors de cette transition, et enfin voir les techniques conseillées pour faire cette migration. 



   ### Introduction
   Parmi les tâches des développeurs dans une entreprise, c’est de développer des applications qui seront utilisés par des clients. Au début, ces applications ne prennent pas en charge beaucoup de ressource, donc l’utilisation d’une architecture monolithique est préférable et un petit nombre de développeur peuvent développer ces applications le plus rapidement possible et de les déployer sans souci.  

   Au fur et à mesure que de nouvelles fonctionnalités sont ajoutées sur ces applications, par exemple, l’utilisation des applications par des ordinateurs de bureaux, par un téléphone mobile ou l’utilisation d’autre service web pour autoriser le traitement des paiements par carte de crédit, l’équipe du développement augmente. Avoir plusieurs équipes de développeurs qui travailleront sur ces applications monolithique peut s’avérer peiné, beaucoup de conflit de code le plus souvent. Il devient plus difficile de maintenir, à développer, à faire évoluer et à modifier ces applications. Cet ainsi que l’équipe de développeur envisagera de migrer aux microservices.  

   Cependant, la migration de l’architecture monolithique vers les microservices n'est pas une opération simple. Les entreprises se lance généralement la migration sans aucune expérience des microservices et ignorent les défis qui les attendent. 

   C’est dans ce cadre s’inscrit ce mémoire de recherche. Nous allons voir dans un premier temps, l’architecture monolithique, parler de ses avantages et de ses inconvénients. Ensuite, parler dans un second temps des microservices, de ses avantages et de ses inconvénients. Enfin présenter des approches pratiques qui peuvent être pratiquer pour relever ces nouveaux défis pendant cette migration. 

2) Architecture monolithique 
 
 Une architecture monolithique est un modèle traditionnel de programme de développement, conçu comme une unité unifiée autonome et indépendante d'autres apps. Le terme « monolithe » est souvent attribué à un objet volumineux et froid, ce qui représente assez bien une architecture monolithique pour la conception de logiciels. Une architecture monolithique constitue un vaste réseau informatique unique doté d'une base de code qui associe toutes les préoccupations de l'entreprise. Pour apporter un changement à ce type d'app, il faut mettre à jour l'ensemble de la stack en accédant à la base de code, puis en créant et en déployant une version mise à jour de l'interface côté service. Les mises à jour sont alors contraignantes et chronophages.Les monolithes peuvent être pratiques durant les premières phases d'un projet pour faciliter la charge cognitive liée à la gestion du code et au déploiement. Cela permet de livrer tout le contenu du monolithe à la fois. (1)  
 Cette architecture est souvent utilisée par les développeurs car  elle est simple,et  un bon moyen de débuter le développement. Cette architecture est composé d’une couche d'interface utilisateur,d'une couche de logique métier et d'une couche d'accès aux données.
 Le schéma suivant illustre un exemple de configuration d'application e-commerce dans lequel l'application est composée de divers modules. Dans une application monolithique, les modules sont définis à l'aide d'une combinaison de constructions de langages de programmation (par exemple, les packages Java) et d'artefacts de compilation (par exemple, les fichiers JAR Java).(5) 

 Figure 1 : Schéma d'une application e-commerce monolithique avec plusieurs modules utilisant une combinaison de constructions de langages de programmation. (6) 

Dans la figure 1, les différents modules de l'application e-commerce correspondent à la logique métier pour la gestion des paiements, de la livraison et des commandes. Tous ces modules sont empaquetés et déployés sous la forme d'un exécutable logique unique. Le format réel dépend du langage et du framework de l'application.  

 

Il existe au moins trois types de systèmes monolithiques : le système à processus unique (the single-process system), le monolithe distribué (the distributed monolith) et les systèmes de boîte noire tiers (black-box systems) 

a)  Le système à processus unique : 

L'exemple le plus courant qui vient à l'esprit lorsque l'on parle de monolithes est un système dans lequel tout le code est déployé en un seul processus, comme dans la figure 1-6. Vous pouvez avoir plusieurs instances de ce processus pour des raisons de robustesse ou de mise à l'échelle, mais fondamentalement, tout le code est regroupé dans un seul processus. En réalité, ces systèmes à processus unique peuvent être de simples systèmes distribués à part entière, car ils finissent presque toujours par lire ou stocker des données dans une base de données.(2) 

b)  Le monolithe distribué : 

Un monolithe distribué est un système composé de plusieurs services, mais pour une raison quelconque, l'ensemble du système doit être déployé ensemble. Un monolithe distribué peut très bien répondre à la définition d'une architecture orientée services, mais échoue trop souvent à tenir les promesses de la SOA. (3) 

Les monolithes distribués émergent généralement dans un environnement où l'accent n'était pas suffisamment mis sur des concepts tels que la dissimulation d'informations et la cohésion des fonctionnalités de l'entreprise, conduisant à la place à des architectures hautement couplées dans lesquelles les changements se répercutent sur les frontières de service, et des changements apparemment innocents qui semblent être locaux dans la rupture de portée. autres parties du système. (4) 

c)  Les systèmes de boîte noire tiers: 

 
Nous pouvons également considérer certains logiciels tiers comme des monolithes que nous pouvons vouloir « décomposer » dans le cadre d'un effort de migration. Ceux-ci peuvent inclure des éléments tels que les systèmes de paie, les systèmes CRM et les systèmes RH. Le facteur commun ici est qu'il s'agit d'un logiciel développé par d'autres personnes et que vous n'avez pas la possibilité de modifier le code. Il peut s'agir d'un logiciel prêt à l'emploi que vous avez déployé sur votre propre infrastructure ou d'un produit Software-as-a-Service (SaaS) que vous utilisez. La plupart des techniques de décomposition que nous allons explorer dans ce livre peuvent être utilisées même avec des systèmes où vous ne pouvez pas modifier le code sous-jacent. 
 

